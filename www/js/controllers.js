app.controller('appController', function ($scope, $state, $ionicPlatform, $window, $ionicSlideBoxDelegate, $ionicPopup, $timeout) {

    $scope.labels = ["", "", ""];
    $scope.data = [45, 55, 1];

    /*    $scope.linelabels = [2013, 2060, 2110];
        $scope.linedata = [
            [127.3, 105.45, 96.61],
            [127.3, 86.74, 42.86]
        ];

        $scope.colors = [{
            fillColor: 'rgba(68,126,188,0)',
            strokeColor: 'rgba(68,126,188,1)',
            pointColor: 'rgba(68,126,188,1)',
            pointStrokeColor: '#fff',
            pointHighlightFill: '#fff',
            pointHighlightStroke: 'rgba(68,126,188,0.8)'
        }, {
            fillColor: 'rgba(238,66,61,0)',
            strokeColor: 'rgba(238,66,61,1)',
            pointColor: 'rgba(238,66,61,1)',
            pointStrokeColor: '#fff',
            pointHighlightFill: '#fff',
            pointHighlightStroke: 'rgba(238,66,61,0.8)'
        }];*/


    //icon list
    $scope.masterObj = {
        page1: {
            terucolo: false,
            multi: false,
            container: false
        },
        page2: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            popupMessage: "Your blood has several components."
        },
        page3: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            popupMessage: "Together, they make up whole blood."
        },
        page4: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            popupMessage: "Your blood donation is powerful."
        },
        page5: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true
        },
        page6: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            popupMessage: "Blood donors are like super heroes."
        },
        page7: {
            icon: "star",
            terucolo: false,
            multi: false,
            container: false
        },
        page8: {
            icon: "circle",
            terucolo: true,
            multi: true,
            total: null,
            swipeCount: 0,
            container: true,
            popupMessage: "There are different ways to donate."
        },
        page9: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            logo: "/android_asset/www/img/logo_small.png"
        },
        page10: {
            icon: "circle",
            terucolo: true,
            multi: true,
            total: null,
            swipeCount: 0,
            container: true,
            logo: "/android_asset/www/img/logo_small.png",
            subpages: {
                2: {
                    msg: "A whole blood donation helps patients who need red blood cells and plasma.",
                    top: "70px"
                }
            }
        },
        page11: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            logo: "/android_asset/www/img/logo_small.png"
        },
        page12: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true
        },
        page13: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            top: "80px",
            popupMessage: "Wouldn't it be nice if you could donate only platelet cells? You can!"
        },
        page14: {
            icon: "circle",
            terucolo: true,
            multi: true,
            total: null,
            swipeCount: 0,
            container: true
        },
        page15: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            logo: "/android_asset/www/img/logo_large.png"
        },
        page16: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            logo: "/android_asset/www/img/logo_large.png"
        },
        page17: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            logo: "/android_asset/www/img/logo_large.png"
        },
        page18: {
            icon: "circle",
            terucolo: true,
            multi: false,
            container: true,
            logo: "/android_asset/www/img/logo_large.png",
            top: "90px",
            popupMessage: "Now, let's take you step-by-step through an apheresis procedure."
        },
        page19: {
            icon: "circle",
            terucolo: true,
            multi: true,
            total: null,
            swipeCount: 0,
            container: true,
            subpages: {
                0: {
                    msg: "Pumps push the blood through the sterile tubing, not through the device. Your blood never touches the device.",
                    top: "45px"
                },
                1: {
                    msg: "This prevents your blood from creating a clot during the collection.",
                    top: "85px"
                },
                2: {
                    msg: "Ask the nurse if you have any questions. Nurses are experts and can help you feel comfortable.",
                    top: "70px"
                },
                3: {
                    msg: "Only a light movement is needed.",
                    top: "120px"
                },
                4: {
                    msg: "You will hear pumps turning and the centrifuge speed will begin increasing. All of the sounds are normal.",
                    top: "45px"
                },
                5: {
                    msg: "Ask a nurse for a blanket to stay warm if you feel cold.",
                    top: "100px"
                },
                6: {
                    msg: "If you have donated blood before on another system, this may feel a little different, but that is OK!",
                    top: "50px"
                },
                7: {
                    msg: "Feeling a slight tingle or numbness around your lips is normal and safe.",
                    top: "85px"
                },
                8: {
                    msg: "If you are feeling uncomfortable, please tell a nurse. The nurse can make adjustments to the machine and may offer a beverage.",
                    top: "30px"
                },
                9: {
                    msg: "Congratulations! You have done a great job of helping someone’s mother, father, brother, sister, aunt or uncle to get better.",
                    top: "50px"
                },
                10: {
                    msg: "Bring a friend with you next time that you donate and help even more patients!",
                    top: "90px"
                }
            }
        },
        page20: {
            icon: "star",
            terucolo: false,
            multi: false,
            container: false
        },
        page21: {
            terucolo: false,
            multi: false,
            container: true
        },
        page22: {
            terucolo: false,
            multi: false,
            container: true
        }
    };

    //boolean for showing pager
    $scope.showPage = true;
    $scope.showPager = function (page) {
        return !$scope.masterObj[page]["icon"];
    };

    $scope.showTerucolo = true;
    $scope.showTerucoloIcon = function (page) {
        return !$scope.masterObj[page]["terucolo"];
    };

    $scope.showContainer = true;
    $scope.showContainerWrapper = function (page) {
        return !$scope.masterObj[page]["container"];
    };

    $scope.showPopupOnPage = false;
    $scope.showPop = function (page) {
        return $scope.masterObj[page]["popupMessage"];
    };

    $scope.getPopupMessage = function (page) {
        return $scope.masterObj[page]["popupMessage"] || '';
    };

    $scope.showLogo = '';
    $scope.getLogo = function (page) {
        return $scope.masterObj[page]["logo"] || '';
    };

    /*    //obj to hold time stops for information
        $scope.videoList = {
            page2: {
                count: 0,
                timestop: {
                    5: "Here is where some shit happens",
                    7: "",
                    10: "Here is where some more shitttttt happens",
                    15: "",
                    20: "Here is like some dope ass shit",
                    35: ""
                }
            },
            page3: {
                count: 0,
                timestop: {
                    2: "Hey fuck you",
                    5: "",
                    10: "No FUCK YOUUUU",
                    12: "",
                    15: "Fuckity fuck fuck",
                    25: ""
                }
            }
        };*/

    //start current view at 1
    $scope.currentView = 1;

    //get the amount of views in application
    $scope.viewLength = Object.keys($scope.masterObj).length;

    //swipeRight event
    $scope.swipeRight = function () {

        if ($scope.masterObj["page" + $scope.currentView].multi) {
            if ($scope.masterObj["page" + $scope.currentView].swipeCount !== 0) {
                $scope.$broadcast("hswiperight");
                if ($scope.masterObj["page" + $scope.currentView].subpages) {
                    $scope.showPopupOnPage = false;
                    var timer = $timeout(function () {
                        if ($scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount]) {
                            if ($scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].msg) {
                                $scope.popupMessage = $scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].msg;
                                $scope.showPopupOnPage = true;
                            }
                            if ($scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].top) {
                                $scope.$broadcast("reposition", $scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].top)
                            }
                        }

                        $timeout.cancel(timer);
                    }, 500);
                }
                return;
            }
        }

        //iterate currentView
        $scope.currentView--;

        //set the new location
        $state.go('page' + $scope.currentView);

        //pagechange event
        $scope.$broadcast("pagechange");

    };

    $scope.$on("nextpage", function () {
        $scope.currentView++;
        $state.go('page' + $scope.currentView);
    });
    $scope.$on("lastpage", function () {
        $scope.currentView--;
        $state.go('page' + $scope.currentView);
    });

    //swipeLeft event
    $scope.swipeLeft = function () {

        if ($scope.masterObj["page" + $scope.currentView].multi) {

            if ($scope.masterObj["page" + $scope.currentView].total !== $scope.masterObj["page" + $scope.currentView].swipeCount) {
                $scope.$broadcast("hswipeleft");
                if ($scope.masterObj["page" + $scope.currentView].subpages) {
                    $scope.showPopupOnPage = false;
                    var timer = $timeout(function () {
                        if ($scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount]) {
                            if ($scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].msg) {
                                $scope.popupMessage = $scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].msg;
                                $scope.showPopupOnPage = true;
                            }
                            if ($scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].top) {
                                $scope.$broadcast("reposition", $scope.masterObj["page" + $scope.currentView].subpages[$scope.masterObj["page" + $scope.currentView].swipeCount].top)
                            }
                        }

                        $timeout.cancel(timer);
                    }, 500);
                }
                return;
            }
        }

        //iterate currentView
        $scope.currentView++;

        //set the new location
        $state.go('page' + $scope.currentView);

        //pagechange event
        $scope.$broadcast("pagechange");

    };

    $scope.swipeUp = function () {
        $scope.$broadcast("swipeup");
    };
    $scope.swipeDown = function () {
        $scope.$broadcast("swipedown");
    };

    $scope.$on('$stateChangeSuccess', function (a, toPage) {

        $scope.showPopupOnPage = false;

        var timer = $timeout(function () {
            if ($scope.masterObj[toPage.name].top) {
                $scope.$broadcast("reposition", $scope.masterObj[toPage.name].top);
            } else {
                $scope.$broadcast("reposition", null);
            }

            if ($scope.masterObj[toPage.name].subpages) {
                if ($scope.masterObj[toPage.name].subpages[$scope.masterObj[toPage.name].swipeCount]) {
                    $scope.$broadcast("reposition", $scope.masterObj[toPage.name].subpages[$scope.masterObj[toPage.name].swipeCount].top);
                    $scope.popupMessage = $scope.masterObj[toPage.name].subpages[$scope.masterObj[toPage.name].swipeCount].msg;
                    $scope.showPopupOnPage = true;
                }
            } else {
                $scope.popupMessage = $scope.getPopupMessage(toPage.name);
                $scope.showPopupOnPage = $scope.showPop(toPage.name);
            }

            $timeout.cancel(timer);
        }, 500);

        $scope.showPage = $scope.showPager(toPage.name);
        $scope.showTerucolo = $scope.showTerucoloIcon(toPage.name);
        $scope.showContainer = $scope.showContainerWrapper(toPage.name);
        $scope.showLogo = $scope.getLogo(toPage.name);


        /*        if (toPage.name === "gem_game") {
                    screen.lockOrientation("portrait-primary");
                } else {
                    screen.lockOrientation("landscape-primary");
                }*/
    });

    //pause event
    $ionicPlatform.on("pause", function () {
        $ionicPopup.confirm({
            title: 'Restart Session',
            template: 'Would you like to restart the session?'
        }).then(function (res) {
            if (res) {
                $window.location.href = "";
            }
        });
    });

    //go to game
    $scope.goToGame = function (game) {
        $state.go(game);
    };

    //back to app
    $scope.backToApp = function (page) {
        $state.go('page' + page);
        $scope.currentView = page;
        $scope.$broadcast("refresh");
    };

    $scope.disableSwipe = function () {
        $ionicSlideBoxDelegate.enableSlide(false);
    };

    $scope.next = function () {
        $ionicSlideBoxDelegate.next();
    };

    $scope.previous = function () {
        $ionicSlideBoxDelegate.previous();
    };

    //numberclick (apheresis)
    $scope.numberClick = function (num) {
        var value = num - $scope.masterObj["page" + $scope.currentView].swipeCount;
        if (value === 0) {
            return;
        }
        if (value > 0) {
            for (var i = 0; i < value; i++) {
                $scope.swipeLeft();
            }
        } else {
            for (var j = 0; j < -value; j++) {
                $scope.swipeRight();
            }
        }
    };
});