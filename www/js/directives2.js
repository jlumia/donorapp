//text fade in
app.directive('textfadein', function ($interval) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var txt = element.find("div");
            var count = 0;
            var int = $interval(function () {
                angular.element(txt[count]).addClass("showText");

                if (count === (txt.length - 1)) {
                    $interval.cancel(int);
                }
                count++;
            }, 500);
        }
    };
});

//image fade in
app.directive('imgfadein', function ($interval) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var imgs = element.find("img");
            var count = 0;
            var int = $interval(function () {
                angular.element(imgs[count]).addClass("showImg");

                if (count === (imgs.length - 1)) {
                    $interval.cancel(int);
                }
                count++;
            }, 750);
        }
    };
});

//vertical swipe
app.directive('verticalswipe', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {

            //get the container for the images
            var imgContainer = angular.element(element.children()[0]);

            //starting position height
            var posHeight = 0;

            //get height of div
            var divHeight = imgContainer.children()[0].clientHeight;

            //add all the heights up for the total swipe height
            var totalHeight = 0;
            angular.forEach(imgContainer.children(), function (v) {
                totalHeight = totalHeight - v.clientHeight;
            });

            //swipe up event
            scope.$on("swipeup", function () {

                //bail out if were at the bottom
                if ((totalHeight - (-divHeight)) === posHeight) {
                    scope.$emit("nextpage");
                    return;
                }

                //subtract height
                posHeight = posHeight - divHeight;

                //apply css
                imgContainer.css("margin-top", posHeight + "px");

            });

            //swipe down event
            scope.$on("swipedown", function () {

                //bail out if were at the top
                if (posHeight === 0) {
                    return;
                }

                //add height
                posHeight = posHeight + divHeight;

                //apply css
                imgContainer.css("margin-top", posHeight + "px");
            });
        }
    };
});

//horizontal swipe
app.directive('horizontalswipe', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {

            //get the container to be swiped through
            var swipeContainer = element.children().children();

            //get the width of each div
            var divWidth = swipeContainer.children()[0].clientWidth;

            //add the divs up for the total
            var totalWidth = 0;
            angular.forEach(swipeContainer.children(), function (v, i) {
                totalWidth = totalWidth + v.clientWidth;
            });

            //get the end position
            var totalCount = swipeContainer.children().length - 1;
            scope.masterObj["page" + scope.currentView].total = totalCount;

            //assign the width to the swipe container
            swipeContainer.css("width", totalWidth + "px");

            //assign margin-left from the masterObj
            swipeContainer.css("margin-left", (scope.masterObj["page" + scope.currentView].swipeCount * -divWidth) + "px");

            //get the span
            var span = angular.element(swipeContainer.find("span")[scope.masterObj["page" + scope.currentView].swipeCount]);

            //assign the transition .2s after the page load (looks wierd otherwise)
            var timer = $timeout(function () {
                swipeContainer.css("transition", "margin-left .5s");
                $timeout.cancel(timer);
            }, 200);

            //declare text
            var text = null;
            var imgFade = null;
            if (imgFade) {
                imgFade.removeClass("showText");
            }
            if (span.find("img").length > 0) {
                imgFade = angular.element(span.find("img")[0]);
                if (imgFade.hasClass("fadeIn")) {
                    imgFade.addClass("showText");
                }
            }

            //handle text
            if (text) {
                text.removeClass("popupShow");
            }
            if (span.find("p").length > 0) {
                text = angular.element(span.find("p"));
                text.addClass("popupShow");
            }

            //left swipe event (forward)
            scope.$on("hswipeleft", function () {

                //bail out if at the end
                if (totalCount === scope.masterObj["page" + scope.currentView].swipeCount) {
                    return;
                }

                //iterate swipe count
                scope.masterObj["page" + scope.currentView].swipeCount++;

                //assign span
                span = angular.element(swipeContainer.find("span")[scope.masterObj["page" + scope.currentView].swipeCount]);

                //handle text
                if (text) {
                    text.removeClass("popupShow");
                }
                if (span.find("p").length > 0) {
                    text = angular.element(span.find("p"));
                    var t = $timeout(function () {
                        text.addClass("popupShow");
                        $timeout.cancel(t);
                    }, 500);
                }
                if (imgFade) {
                    imgFade.removeClass("showText");
                }
                if (span.find("img").length > 0) {
                    imgFade = angular.element(span.find("img")[0]);
                    if (imgFade.hasClass("fadeIn")) {
                        swipeContainer.css("transition", "margin-left 0s");
                        var t2 = $timeout(function () {
                            swipeContainer.css("margin-left", (-divWidth * scope.masterObj["page" + scope.currentView].swipeCount) + "px");
                            imgFade.addClass("showText");
                            swipeContainer.css("transition", "margin-left .5s");
                            $timeout.cancel(t2);
                        }, 500);
                    } else {
                        swipeContainer.css("margin-left", (-divWidth * scope.masterObj["page" + scope.currentView].swipeCount) + "px");
                    }
                }

                //assign the margin-left to the css, show the swipe
                swipeContainer.css("margin-left", (-divWidth * scope.masterObj["page" + scope.currentView].swipeCount) + "px");

            });

            //right swipe event
            scope.$on("hswiperight", function () {

                //bail out if at the beginning
                if (scope.masterObj["page" + scope.currentView].swipeCount === 0) {
                    return;
                }

                //iterate swipe count
                scope.masterObj["page" + scope.currentView].swipeCount--;

                //assign span
                span = angular.element(swipeContainer.find("span")[scope.masterObj["page" + scope.currentView].swipeCount]);

                //handle text
                if (text) {
                    text.removeClass("popupShow");
                }
                if (span.find("p").length > 0) {
                    text = angular.element(span.find("p"));
                    var t = $timeout(function () {
                        text.addClass("popupShow");
                        $timeout.cancel(t);
                    }, 500);
                }
                if (imgFade) {
                    imgFade.removeClass("showText");
                }
                if (span.find("img").length > 0) {
                    imgFade = angular.element(span.find("img")[0]);
                    if (imgFade.hasClass("fadeIn")) {
                        var t2 = $timeout(function () {
                            swipeContainer.css("transition", "margin-left 0s");
                            swipeContainer.css("margin-left", (-divWidth * scope.masterObj["page" + scope.currentView].swipeCount) + "px");
                            imgFade.addClass("showText");
                            swipeContainer.css("transition", "margin-left .5s");
                            $timeout.cancel(t2);
                        }, 500);
                    } else {
                        swipeContainer.css("margin-left", (-divWidth * scope.masterObj["page" + scope.currentView].swipeCount) + "px");
                    }
                }

                //assign the margin-left to the css, show the swipe

            });
        }
    };
});

//wide popup
app.directive('widepopup', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            angular.element(element.children()[2]).on("click", function () {
                element.removeClass("popupShow");
            });
        }
    };
});

//popup
app.directive('popup', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            scope.$on('reposition', function (e, val) {
                var v = val || "120px";
                element.css("top", v);
            });
        }
    };
});

//directive for games
app.directive('game', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {

            //set height/width
            element.css("height", window.innerHeight + 'px');
            element.css("width", "100%");

            //reload iframe when navigating away from game
            scope.$on('refresh', function () {
                element.attr('src', element.attr('src'));
            });
        }
    };
});

//directive for video elements
app.directive('customvideo', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {

            //declare variables: current video playing, the video element, and the play/pause overlay
            var currentVideo = null;
            var videoEl = null;
            var videoOverlay = angular.element(element.children()[0]);

            //listen for play
            element.bind("touchstart", function () {

                //get the timestop info div
                var infoMessage = angular.element(element.parent().children()[1]);

                //add class to hide overlay
                videoOverlay.addClass("customHide");

                //assign current video
                currentVideo = scope.videoList["page" + scope.currentView];

                //get the video element
                videoEl = element.find("video")[0];

                //if paused, play video
                if (videoEl.paused) {

                    //play
                    videoEl.play();

                    //timestops for information
                    videoEl.ontimeupdate = function () {

                        //get position
                        var position = Object.keys(currentVideo.timestop)[currentVideo.count];

                        //show each info on timestop
                        if (position < videoEl.currentTime) {

                            if (currentVideo.timestop[position].length > 0) {
                                scope.message = currentVideo.timestop[position];
                                scope.$apply();
                                infoMessage.addClass("infoMessageShow");
                            } else {
                                infoMessage.removeClass("infoMessageShow");
                                infoMessage.one("transitionend", function () {

                                    //set message
                                    scope.message = currentVideo.timestop[position];
                                    scope.$apply();
                                });
                            }

                            //iterate count
                            currentVideo.count++;

                            //if time is over last timestop, kill timer
                            if (currentVideo.count === Object.keys(currentVideo.timestop).length) {
                                videoEl.ontimeupdate = null;
                            }
                        }
                    };
                } else {

                    //remove overlay hide class
                    videoOverlay.removeClass("customHide");

                    //pause
                    videoEl.pause();
                }
            });

            //listen for view change and restart video
            scope.$on("pagechange", function () {
                if (currentVideo) {

                    //remove overlay hide class
                    videoOverlay.removeClass("customHide");

                    //reload video
                    videoEl.load();

                    //reset count and message
                    currentVideo.count = 0;
                    scope.message = '';
                }
            });
        }
    };
});