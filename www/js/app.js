var app = angular.module('donorApp', ['ionic', 'chart.js'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('page1', {
                url: "/page1",
                templateUrl: "templates/page1.html"
            }).state('page2', {
                url: "/page2",
                templateUrl: "templates/page2.html"
            }).state('page3', {
                url: "/page3",
                templateUrl: "templates/page3.html"
            }).state('page4', {
                url: "/page4",
                templateUrl: "templates/page4.html"
            }).state('page5', {
                url: "/page5",
                templateUrl: "templates/page5.html"
            }).state('page6', {
                url: "/page6",
                templateUrl: "templates/page6.html"
            }).state('page7', {
                url: "/page7",
                templateUrl: "templates/page7.html"
            }).state('page8', {
                url: "/page8",
                templateUrl: "templates/page8.html"
            }).state('page9', {
                url: "/page9",
                templateUrl: "templates/page9.html"
            }).state('page10', {
                url: "/page10",
                templateUrl: "templates/page10.html"
            }).state('page11', {
                url: "/page11",
                templateUrl: "templates/page11.html"
            }).state('page12', {
                url: "/page12",
                templateUrl: "templates/page12.html"
            }).state('page13', {
                url: "/page13",
                templateUrl: "templates/page13.html"
            }).state('page14', {
                url: "/page14",
                templateUrl: "templates/page14.html"
            }).state('page15', {
                url: "/page15",
                templateUrl: "templates/page15.html"
            }).state('page16', {
                url: "/page16",
                templateUrl: "templates/page16.html"
            }).state('page17', {
                url: "/page17",
                templateUrl: "templates/page17.html"
            }).state('page18', {
                url: "/page18",
                templateUrl: "templates/page18.html"
            }).state('page19', {
                url: "/page19",
                templateUrl: "templates/page19.html"
            }).state('page20', {
                url: "/page20",
                templateUrl: "templates/page20.html"
            }).state('page21', {
                url: "/page21",
                templateUrl: "templates/page21.html"
            }).state('page22', {
                url: "/page22",
                templateUrl: "templates/page22.html"
            }).state('spin_game', {
                url: "/spin_game",
                templateUrl: "templates/spin_game.html"
            }).state('gem_game', {
                url: "/gem_game",
                templateUrl: "templates/gem_game.html"
            });

        $urlRouterProvider.otherwise("/page1");

    });